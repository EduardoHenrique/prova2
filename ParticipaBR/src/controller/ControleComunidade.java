package controller;

import java.util.ArrayList;

import model.Comunidade;

public class ControleComunidade {
	
	private ArrayList<Comunidade> listaComunidade;

	public ControleComunidade(ArrayList<Comunidade> listaComunidade) {
		this.listaComunidade = new ArrayList<Comunidade>();
	}

	public ArrayList<Comunidade> getListaComunidade() {
		return listaComunidade;
	}

	public void setListaComunidade(ArrayList<Comunidade> listaComunidade) {
		this.listaComunidade = listaComunidade;
	}
	
	
}
