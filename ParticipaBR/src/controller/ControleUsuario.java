package controller;

import java.util.ArrayList;

import model.Governo;
import model.Moderador;
import model.Usuario;

public class ControleUsuario {
	private ArrayList<Usuario> listaUsuario;
	private ArrayList<Moderador> listaModerador;
	private ArrayList<Governo> listaGoverno;
	
	public ControleUsuario(ArrayList<Usuario> listaUsuario,
			ArrayList<Moderador> listaModerador, ArrayList<Governo> listaGoverno) {
		this.listaUsuario = new ArrayList<Usuario>();
		this.listaModerador = new ArrayList<Moderador>();
		this.listaGoverno = new ArrayList<Governo>();
	}

	public ArrayList<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(ArrayList<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

	public ArrayList<Moderador> getListaModerador() {
		return listaModerador;
	}

	public void setListaModerador(ArrayList<Moderador> listaModerador) {
		this.listaModerador = listaModerador;
	}

	public ArrayList<Governo> getListaGoverno() {
		return listaGoverno;
	}

	public void setListaGoverno(ArrayList<Governo> listaGoverno) {
		this.listaGoverno = listaGoverno;
	}
	
	
	
	
} 
