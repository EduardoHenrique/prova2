package controller;

import java.util.ArrayList;

import model.Duvida;
import model.Elogio;
import model.Reclamacao;

public class ControleObservacao {
	private ArrayList<Duvida> listaDuvida;
	private ArrayList<Reclamacao> listaReclamacao;
	private ArrayList<Elogio> listaElogio;
	
	public ControleObservacao(ArrayList<Duvida> listaDuvida,
			ArrayList<Reclamacao> listaReclamacao, ArrayList<Elogio> listaElogio) {
		this.listaDuvida = new ArrayList<Duvida>();
		this.listaReclamacao = new ArrayList<Reclamacao>();
		this.listaElogio = new ArrayList<Elogio>();
	}

	public ArrayList<Duvida> getListaDuvida() {
		return listaDuvida;
	}

	public void setListaDuvida(ArrayList<Duvida> listaDuvida) {
		this.listaDuvida = listaDuvida;
	}

	public ArrayList<Reclamacao> getListaReclamacao() {
		return listaReclamacao;
	}

	public void setListaReclamacao(ArrayList<Reclamacao> listaReclamacao) {
		this.listaReclamacao = listaReclamacao;
	}

	public ArrayList<Elogio> getListaElogio() {
		return listaElogio;
	}

	public void setListaElogio(ArrayList<Elogio> listaElogio) {
		this.listaElogio = listaElogio;
	}
		
}
