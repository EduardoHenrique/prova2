package teste;

import static org.junit.Assert.*;

import model.Enquete;
import model.Moderador;

import org.junit.Test;
import org.junit.Before;

public class TesteEnquete {
	private static String titulo;
	private static int votoA;
	private static int votoB;
	private static int votoC;
	private static int votoD;
	private static Moderador umModerador;
	private static Enquete umaEnquete;
	
	@Before
	public void setUp() throws Exception{
		titulo = "Título Enquete";
		votoA = 1;
		votoB = 2;
		votoC = 3;
		votoD = 4;
		umModerador = new Moderador("Nome", "email", "senha");
		umaEnquete = new Enquete(titulo, umModerador);
		umaEnquete.setVotoA(votoA);
		umaEnquete.setVotoB(votoB);
		umaEnquete.setVotoC(votoC);
		umaEnquete.setVotoD(votoD);
	}
	
	@Test
	public void testTituloEnquete() {
		assertEquals("Título Enquete", umaEnquete.getTitulo());
	}
	
	@Test
	public void testNumeroVotos(){
		assertEquals(1,umaEnquete.getVotoA());
		assertEquals(2,umaEnquete.getVotoB());
		assertEquals(3,umaEnquete.getVotoC());
		assertEquals(4,umaEnquete.getVotoD());
	}

}
