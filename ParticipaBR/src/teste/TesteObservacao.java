package teste;

import static org.junit.Assert.*;

import model.Observacao;
import model.Usuario;

import org.junit.Test;
import org.junit.Before;

public class TesteObservacao {
	private static Usuario umUsuario;
	private static Observacao umaObservacao;
	
	@Before
	public void setUp() throws Exception{
		umUsuario = new Usuario("Eduardo", "email", "senha");
		umaObservacao = new Observacao(umUsuario);
	}
	
	@Test
	public void testUsuario() {
		assertEquals(umUsuario, umaObservacao.getUmUsuario());
	}
	@Test
	public void testNomeUsuario(){
		assertEquals("Eduardo", umaObservacao.getUmUsuario().getNome());
	}
	@Test
	public void testEmailUsuario(){
		assertEquals("email", umaObservacao.getUmUsuario().getEmail());
	}
	@Test
	public void testSenhaUsuario(){
		assertEquals("senha", umaObservacao.getUmUsuario().getSenha());
	}
}
