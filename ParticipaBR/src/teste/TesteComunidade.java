package teste;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Comentario;
import model.Comunidade;
import model.Enquete;
import model.Moderador;
import model.Post;
import model.Usuario;

import org.junit.Test;
import org.junit.Before;

public class TesteComunidade {
	private static Usuario umUsuario;
	private static ArrayList<Usuario> listaUsuario;
	private static Comentario umComentario;
	private static ArrayList<Comentario> listaComentario;
	private static Post umPost;
	private static ArrayList<Post> listaPost;
	private static Enquete umaEnquete;
	private static ArrayList<Enquete> listaEnquete;
	private static Moderador umModerador;
	private static Comunidade umaComunidade;
	
	@Before
	public void setUp() throws Exception{
		umUsuario = new Usuario("nome", "email", "senha");
		listaComentario = new ArrayList<Comentario>();
		umModerador = new Moderador("nome", "email", "senha");
		umPost = new Post(listaComentario, umModerador);
		umPost.adicionarComentario(umComentario);
		umaEnquete = new Enquete("Título", umModerador);
		umaComunidade = new Comunidade(listaUsuario, listaPost, listaEnquete);
	}

	@Test
	public void testListaUsuario() {
		assertEquals(0,umaComunidade.getListaUsuario().size());
		umaComunidade.adicionarUsuario(umUsuario);
		assertEquals(1,umaComunidade.getListaUsuario().size());
		umaComunidade.removerUsuario(umUsuario);
		assertEquals(0,umaComunidade.getListaUsuario().size());
	}
	@Test
	public void testListaPost() {
		assertEquals(0,umaComunidade.getListaPost().size());
		umaComunidade.adicionarPost(umPost);
		assertEquals(1,umaComunidade.getListaPost().size());
		umaComunidade.removerPost(umPost);
		assertEquals(0,umaComunidade.getListaPost().size());
	}
	@Test
	public void testListaEnquete() {
		assertEquals(0,umaComunidade.getListaEnquete().size());
		umaComunidade.adicionarEnquete(umaEnquete);
		assertEquals(1,umaComunidade.getListaEnquete().size());
		umaComunidade.removerEnquete(umaEnquete);
		assertEquals(0,umaComunidade.getListaEnquete().size());
	}

}
