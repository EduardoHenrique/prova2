package teste;

import static org.junit.Assert.*;

import model.Duvida;
import model.Usuario;

import org.junit.Test;
import org.junit.Before;

public class TesteDuvida {
	private static Usuario umUsuario;
	private static Duvida umaDuvida;
	private static String textoDuvida;
	
	@Before
	public void setUp() throws Exception{
		umUsuario = new Usuario("Eduardo", "email", "senha");
		textoDuvida = "Não entendi";
		umaDuvida = new Duvida(umUsuario, textoDuvida);
	}
	
	@Test
	public void testUsuario() {
		assertEquals(umUsuario, umaDuvida.getUmUsuario());
	}
	@Test
	public void testTextoElogio(){
		assertEquals("Não entendi", umaDuvida.getDuvida());
	}
	@Test
	public void testNomeUsuario(){
		assertEquals("Eduardo", umaDuvida.getUmUsuario().getNome());
	}
	@Test
	public void testEmailUsuario(){
		assertEquals("email", umaDuvida.getUmUsuario().getEmail());
	}
	@Test
	public void testSenhaUsuario(){
		assertEquals("senha", umaDuvida.getUmUsuario().getSenha());
	}
}
