package teste;

import static org.junit.Assert.*;

import model.Reclamacao;
import model.Usuario;

import org.junit.Test;
import org.junit.Before;

public class TesteReclamacao {
	private static Usuario umUsuario;
	private static Reclamacao umaReclamacao;
	
	@Before
	public void setUp() throws Exception{
		umUsuario = new Usuario("Eduardo", "email", "senha");
		umaReclamacao = new Reclamacao(umUsuario, "Muito ruim");
	}
	
	@Test
	public void testUsuario() {
		assertEquals(umUsuario, umaReclamacao.getUmUsuario());
	}
	@Test
	public void testTextoElogio(){
		assertEquals("Muito ruim", umaReclamacao.getReclamacao());
	}
	@Test
	public void testNomeUsuario(){
		assertEquals("Eduardo", umaReclamacao.getUmUsuario().getNome());
	}
	@Test
	public void testEmailUsuario(){
		assertEquals("email", umaReclamacao.getUmUsuario().getEmail());
	}
	@Test
	public void testSenhaUsuario(){
		assertEquals("senha", umaReclamacao.getUmUsuario().getSenha());
	}
}
