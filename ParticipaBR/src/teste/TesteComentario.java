package teste;

import static org.junit.Assert.*;

import model.Comentario;
import model.Usuario;

import org.junit.Before;
import org.junit.Test;

public class TesteComentario {
	private static Usuario umUsuario;
	private static Comentario umComentario;
	
	@Before
	public void setUp() throws Exception{
		umUsuario = new Usuario("Eduardo", "email", "senha");
		umComentario = new Comentario("Texto de Comentário", umUsuario);
	}

	@Test
	public void testTextoComentario() {
		assertEquals("Texto de Comentário", umComentario.getText());
	}
	@Test
	public void testNomeUsuario(){
		assertEquals("Eduardo", umComentario.getUmUsuario().getNome());
	}
	@Test
	public void testEmailUsuario(){
		assertEquals("email", umComentario.getUmUsuario().getEmail());
	}
	@Test
	public void testSenhaUsuario(){
		assertEquals("senha", umComentario.getUmUsuario().getSenha());
	}
}
