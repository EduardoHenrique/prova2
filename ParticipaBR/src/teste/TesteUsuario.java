package teste;

import static org.junit.Assert.*;

import model.Usuario;

import org.junit.Before;
import org.junit.Test;

public class TesteUsuario {
	private static String nome;
	private static String email;
	private static String senha;
	private static Usuario umUsuario;
	
	@Before
	public void setUp() throws Exception{
		nome = "Eduardo";
		email = "email.com";
		senha = "123";
		umUsuario = new Usuario(nome,email,senha);
	}

	@Test
	public void testNome() {
		assertEquals(nome,umUsuario.getNome());
	}
	@Test
	public void testEmail() {
		assertEquals(email,umUsuario.getEmail());
	}
	@Test
	public void testSenha() {
		assertEquals(senha,umUsuario.getSenha());
	}

}
