package teste;

import static org.junit.Assert.*;

import model.Moderador;

import org.junit.Before;
import org.junit.Test;

public class TesteModerador {
	private static String nome;
	private static String email;
	private static String senha;
	private static Moderador umModerador;
	
	@Before
	public void setUp() throws Exception{
		nome = "Eduardo";
		email = "email.com";
		senha = "123";
		umModerador = new Moderador(nome,email,senha);
	}

	@Test
	public void testNome() {
		assertEquals(nome,umModerador.getNome());
	}
	@Test
	public void testEmail() {
		assertEquals(email,umModerador.getEmail());
	}
	@Test
	public void testSenha() {
		assertEquals(senha,umModerador.getSenha());
	}

}
