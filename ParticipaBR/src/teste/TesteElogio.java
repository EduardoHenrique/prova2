package teste;

import static org.junit.Assert.*;

import model.Elogio;
import model.Usuario;

import org.junit.Test;
import org.junit.Before;

public class TesteElogio {
	private static Usuario umUsuario;
	private static Elogio umElogio;
	private static String textoElogio;
	
	@Before
	public void setUp() throws Exception{
		umUsuario = new Usuario("Eduardo", "email", "senha");
		textoElogio = "Muito bom";
		umElogio = new Elogio(umUsuario, textoElogio);
	}
	
	@Test
	public void testUsuario() {
		assertEquals(umUsuario, umElogio.getUmUsuario());
	}
	@Test
	public void testTextoElogio(){
		assertEquals("Muito bom", umElogio.getElogio());
	}
	@Test
	public void testNomeUsuario(){
		assertEquals("Eduardo", umElogio.getUmUsuario().getNome());
	}
	@Test
	public void testEmailUsuario(){
		assertEquals("email", umElogio.getUmUsuario().getEmail());
	}
	@Test
	public void testSenhaUsuario(){
		assertEquals("senha", umElogio.getUmUsuario().getSenha());
	}
}
