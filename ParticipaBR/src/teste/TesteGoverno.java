package teste;

import static org.junit.Assert.*;

import model.Governo;

import org.junit.Before;
import org.junit.Test;

public class TesteGoverno {
	private static String nome;
	private static String email;
	private static String senha;
	private static String orgao;
	private static Governo umGoverno;
	
	@Before
	public void setUp() throws Exception{
		nome = "Eduardo";
		email = "email.com";
		senha = "123";
		orgao = "Esportes";
		umGoverno = new Governo(nome,email,senha,orgao);
	}

	@Test
	public void testNome() {
		assertEquals(nome,umGoverno.getNome());
	}
	@Test
	public void testEmail() {
		assertEquals(email,umGoverno.getEmail());
	}
	@Test
	public void testSenha() {
		assertEquals(senha,umGoverno.getSenha());
	}
	@Test
	public void testOrgao() {
		assertEquals(orgao,umGoverno.getOrgao());
	}

}
