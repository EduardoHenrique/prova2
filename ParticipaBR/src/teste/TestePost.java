package teste;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Comentario;
import model.Post;
import model.Moderador;
import model.Usuario;

import org.junit.Before;
import org.junit.Test;

public class TestePost {
	private static Moderador umModerador;
	private static Post umPost;
	private static ArrayList<Comentario> listaComentarios;
	private static Comentario primeiroComentario;
	private static Comentario segundoComentario;
	private static Usuario umUsuario;
	
	@Before
	public void setUp() throws Exception{
		umModerador = new Moderador("Eduardo", "email", "senha");
		umUsuario = new Usuario("Fernando", "email2", "senha2");
		listaComentarios = new ArrayList<Comentario>();
	}

	@Test
	public void testListaComentarios() {
		primeiroComentario = new Comentario("Comentário", umUsuario);
		segundoComentario = new Comentario("Comentário 2", umUsuario);
		
		umPost = new Post(listaComentarios, umModerador);
		
		assertEquals(0, umPost.getListaComentarios().size());
		
		umPost.adicionarComentario(primeiroComentario);
		assertEquals(1, umPost.getListaComentarios().size());
		
		umPost.adicionarComentario(segundoComentario);
		assertEquals(2, umPost.getListaComentarios().size());
		
		umPost.removerComentario(primeiroComentario);
		assertEquals(1, umPost.getListaComentarios().size());
		
		assertEquals(listaComentarios, umPost.getListaComentarios());
		
	}
	@Test
	public void testTextoComentario(){
		primeiroComentario = new Comentario("Texto Comentário", umUsuario);
		
		umPost = new Post(listaComentarios, umModerador);
		umPost.adicionarComentario(primeiroComentario);
		
		assertEquals("Texto Comentário", umPost.getListaComentarios().get(0).getText());
	}
	@Test
	public void testNomeModerador(){
		assertEquals("Eduardo", umPost.getUmModerador().getNome());
	}
	@Test
	public void testEmailModerador(){
		assertEquals("email", umPost.getUmModerador().getEmail());
	}
	@Test
	public void testSenhaModerador(){
		assertEquals("senha", umPost.getUmModerador().getSenha());
	}
}