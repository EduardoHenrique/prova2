package model;

import java.util.ArrayList;

public class Post {
	private ArrayList<Comentario> listaComentarios;
	private Moderador umModerador;

	public Post(ArrayList<Comentario> listaComentarios, Moderador umModerador) {
		this.listaComentarios = listaComentarios;
		this.umModerador = umModerador;
	}

	public ArrayList<Comentario> getListaComentarios() {
		return listaComentarios;
	}

	public void setListaComentarios(ArrayList<Comentario> listaComentarios) {
		this.listaComentarios = listaComentarios;
	}

	public Moderador getUmModerador() {
		return umModerador;
	}

	public void setUmModerador(Moderador umModerador) {
		this.umModerador = umModerador;
	}
	
	public void adicionarComentario(Comentario umComentario){
		listaComentarios.add(umComentario);
	}
	
	public void removerComentario(Comentario umComentario){
		listaComentarios.remove(umComentario);
	}
}
