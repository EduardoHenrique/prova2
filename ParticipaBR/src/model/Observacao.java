package model;

public class Observacao {
	private Usuario umUsuario;

	public Observacao(Usuario umUsuario) {
		super();
		this.umUsuario = umUsuario;
	}

	public Usuario getUmUsuario() {
		return umUsuario;
	}

	public void setUmUsuario(Usuario umUsuario) {
		this.umUsuario = umUsuario;
	}
	
	
}
