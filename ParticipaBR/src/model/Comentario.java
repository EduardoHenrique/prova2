package model;

public class Comentario {
	private String text;
	private Usuario umUsuario;

	public Comentario(String text, Usuario umUsuario) {
		this.text = text;
		this.setUmUsuario(umUsuario);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String comentarPost (Post umPost){
		return text;
	}

	public Usuario getUmUsuario() {
		return umUsuario;
	}

	public void setUmUsuario(Usuario umUsuario) {
		this.umUsuario = umUsuario;
	}
}
