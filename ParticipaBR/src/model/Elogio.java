package model;

public class Elogio extends Observacao{
	private String elogio;

	public Elogio(Usuario umUsuario, String elogio) {
		super(umUsuario);
		this.elogio = elogio;
	}

	public String getElogio() {
		return elogio;
	}

	public void setElogio(String elogio) {
		this.elogio = elogio;
	}
	
	
}
