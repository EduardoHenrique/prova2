package model;

public class Enquete {
	private String titulo;
	private int votoA;
	private int votoB;
	private int votoC;
	private int votoD;
	private Moderador umModerador;
	
	public Enquete(String titulo, Moderador umModerador){
		this.titulo = titulo;
		this.umModerador = umModerador;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getVotoA() {
		return votoA;
	}

	public void setVotoA(int votoA) {
		this.votoA = votoA;
	}

	public int getVotoB() {
		return votoB;
	}

	public void setVotoB(int votoB) {
		this.votoB = votoB;
	}

	public int getVotoC() {
		return votoC;
	}

	public void setVotoC(int votoC) {
		this.votoC = votoC;
	}

	public int getVotoD() {
		return votoD;
	}

	public void setVotoD(int votoD) {
		this.votoD = votoD;
	}

	public Moderador getUmModerador() {
		return umModerador;
	}

	public void setUmModerador(Moderador umModerador) {
		this.umModerador = umModerador;
	}
	
}
