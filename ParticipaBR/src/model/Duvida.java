package model;

public class Duvida extends Observacao {
	private String duvida;

	public Duvida(Usuario umUsuario, String duvida) {
		super(umUsuario);
		this.duvida = duvida;
	}

	public String getDuvida() {
		return duvida;
	}

	public void setDuvida(String duvida) {
		this.duvida = duvida;
	}
	
	
}
