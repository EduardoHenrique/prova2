package model;

public class Reclamacao extends Observacao{
	private String reclamacao;

	public Reclamacao(Usuario umUsuario, String reclamacao) {
		super(umUsuario);
		this.reclamacao = reclamacao;
	}

	public String getReclamacao() {
		return reclamacao;
	}

	public void setReclamacao(String reclamacao) {
		this.reclamacao = reclamacao;
	}
	
	
	
}
