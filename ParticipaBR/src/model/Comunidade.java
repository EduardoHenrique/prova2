package model;

import java.util.ArrayList;

public class Comunidade {
	private ArrayList<Usuario> listaUsuario;
	private ArrayList<Post> listaPost;
	private ArrayList <Enquete> listaEnquete;
	
	public Comunidade(ArrayList<Usuario> listaUsuario,
			ArrayList<Post> listaPost, ArrayList<Enquete> listaEnquete) {
		this.listaUsuario = new ArrayList<Usuario>();
		this.listaPost = new ArrayList<Post>();
		this.listaEnquete = new ArrayList<Enquete>();
	}

	public ArrayList<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(ArrayList<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

	public ArrayList<Post> getListaPost() {
		return listaPost;
	}

	public void setListaPost(ArrayList<Post> listaPost) {
		this.listaPost = listaPost;
	}

	public ArrayList<Enquete> getListaEnquete() {
		return listaEnquete;
	}

	public void setListaEnquete(ArrayList<Enquete> listaEnquete) {
		this.listaEnquete = listaEnquete;
	}
	public void adicionarUsuario(Usuario umUsuario){
		listaUsuario.add(umUsuario);
	}
	
	public void removerUsuario(Usuario umUsuario){
		listaUsuario.remove(umUsuario);
	}
	public void adicionarPost(Post umPost){
		listaPost.add(umPost);
	}
	
	public void removerPost(Post umPost){
		listaPost.remove(umPost);
	}
	public void adicionarEnquete(Enquete umaEnquete){
		listaEnquete.add(umaEnquete);
	}
	public void removerEnquete(Enquete umaEnquete){
		listaEnquete.remove(umaEnquete);
	}
	
}
