package model;

public class Governo extends Usuario{
	
	private String orgao;

	public Governo(String nome, String email, String senha, String orgao) {
		super(nome, email, senha);
		this.orgao = orgao;
	}

	public String getOrgao() {
		return orgao;
	}

	public void setOrgao(String orgao) {
		this.orgao = orgao;
	}

}
